// eprime mode: 
//  - LED A-E are eprime on-board lamp 1-5
//  - LED F-H are eprime external lamp 6-8
//  - button A-E are eprime on-board key 1-5
//  - NOT: button F-H are eprime external key 9-11
//  - button F is eprime external key #6 which is also used for the sound key
//  - button G is eprime external key #7 which is for instance for the light sensor high
//  - button H is eprime external key #8
//  - both voice key and sound key trigger voice key/external key #6
//  - 800Hz with 19200 bit/s appears to be the standard setting in ePrime
// extended mode:
// M: set marker 
// P: send pulse
// X: set pulse duration in ms (for P)
// 
///

#include "digitalWriteFast2.h" // c++ compliant version of digitalWriteFast.h
#include <EEPROM.h>
#include <SPI.h>

// version must be set in Makefile by git
#ifndef VERSION
#define VERSION "unknown"
#endif

/* arduino.h:

#define lowByte(w) ((uint8_t) ((w) & 0xff))
#define highByte(w) ((uint8_t) ((w) >> 8))

#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))
*/

const int addressMode = 0; // eprom address where mode is stored
// available modes
const byte modeNone        = 0;
const byte modeBitsi       = 1; // H + A
const byte modeBitsiExtend = 2; // H + B
const byte modePersonal    = 3; // H + C, undocumented mode
const byte modeEprime      = 5; // H + E
const byte modeInitial     = modeNone; // if not modeNone, then change mode at startup

// pin numbers (hardware specific)
const byte pinDataOut     = 11; // MOSI 
const byte pinDataIn      = 12; // MISO 
const byte pinSpiClock    = 13; // Clock 
const byte pinAnalogOut   = 2;
const byte pinLed         = 3;
const byte pinOutput      = 4;
const byte pinAnalogIn    = 5;
const byte pinInput[8]    = {6, 7, 8, 9, 16, 17, 18, 19}; //duemilanove/uno specific digital input pins for buttons
//const byte pinInput[8] = { 6, 7, 8, 9, 20, 21, 22, 23}; //leonardo
const byte pinSpeaker     = 14;

// analog channel for sound key and voice key
const byte channelSound   = 1;
const byte channelVoice   = 4;

const byte marginVoice         = 25;  // margin for detection of voice (higher margin = fewer false positives)
const byte marginSound         = 50;  // margin for detection of sound (higher margin = fewer false positives)
const byte timeDebounce        = 2;   // (ms) dead time of a button after button press or release in ms
const unsigned frametimeEprime = 1200;// (ms) 658; // frame time in us for eprime streaming mode
// note that eprime srbox sends about 760 bytes/s when set to 800 bytes/s

// long is 32 bit
unsigned long t0;                     // (ms) time at start of loop (loops in 2^32 ms = 50 days)
unsigned long t0u;                    // (us) time at end of loop in eprime mode (loops in 72 minutes)
unsigned long tPulse = 100;           // (ms) duration of pulse in bitsi extend mode
unsigned long tPulseEnd = 0xffffffff; // (ms) end time of pulse in bitsi extend mode

// current mode and submode
byte mode               = modeNone; // current mode
byte modeEprimeStreaming= 0;        // eprime submode, 0: not streaming, 1: bank 1, 2: bank 2
char modeExtendFunction = 'N';      // current function in extended mode (!='N' if one character read)
char modeExtendLed      = 'X';      // current led function in extended mode (X: none, I: input, O: output)
char modeExtendDetect   = 'X';      // current detect function in extended mode (X: none, S: sound, V: voice)

byte stateButtons = 0;        // state of buttons, one bit per button, 1 is pressed, 0 is not pressed
byte stateLeds = 0;           // state of LEDs, one bit per LED, 1 is on, 0 is off
unsigned long tButtons[8];    // (ms) time of buttonstate change (t0)

unsigned int voiceLow  = 0xffff; // lowest value during calibration - margin
unsigned int voiceHigh = 0;      // highest value during calibration + margin
unsigned int soundHigh = 0;      // highest sensors value during calibration + margin

// low level functions
void initDigitalInput(){
	for (int pinIndex=0; pinIndex<8; pinIndex++){
		pinMode(pinInput[pinIndex], INPUT); // send 1
		digitalWriteFast2(pinInput[pinIndex], HIGH); 
	}
}

void setOutput(byte b){
	// set value of output 
	digitalWriteFast2(pinOutput,LOW);
	SPI.transfer(b);
	digitalWriteFast2(pinOutput,HIGH);
}

void setLeds(byte b){
	// alter global: stateLeds
	// change state of LEDs
	stateLeds = b;
	digitalWriteFast2(pinLed,LOW);
	SPI.transfer(b);
	digitalWriteFast2(pinLed,HIGH);
}

int readAnalog(byte channel){
	// read analog value from channel (1-4) and return as 16 bit signed int
	SPI.end();
	pinMode(pinDataIn, INPUT);
	pinMode(pinDataOut, OUTPUT);
	pinMode(pinSpiClock, OUTPUT);
	// bit 0-2: dont care
	// bit 3-5: channel (0-)
	// bit 6:   mode
	// bit 7:   start
	///
	byte command = 0b11000000;

	//allow channel selection
	command |= (channel-1)<<3;

	digitalWriteFast2(pinAnalogIn, LOW); // select adc
	// setup bits to be written
	for (byte i=7; i>=3; i--){
		digitalWriteFast2(pinDataOut, command&1<<i);
		//cycle clock
		digitalWriteFast2(pinSpiClock, HIGH);
		digitalWriteFast2(pinSpiClock, LOW);    
	}

	digitalWriteFast2(pinSpiClock,HIGH);    //ignores 2 null bits
	digitalWriteFast2(pinSpiClock,LOW);
	digitalWriteFast2(pinSpiClock,HIGH);
	digitalWriteFast2(pinSpiClock,LOW);

	// read bits from adc MSF
	int adcValue = 0;
	for (int i=11; i>=0; i--){
		adcValue |= digitalReadFast2(pinDataIn)<<i;
		//cycle clock
		digitalWriteFast2(pinSpiClock, HIGH);
		digitalWriteFast2(pinSpiClock, LOW);
	}
	digitalWriteFast2(pinAnalogIn, HIGH); //turn off device
	SPI.begin();
	//adcValue = adcValue >> 4;
	return adcValue;
}

void writeAnalog(byte analog1, byte analog2){  
	// write two bytes (msb, lsb) to analog (which channel?) 
	digitalWriteFast2(pinAnalogOut, LOW);
	SPI.transfer(analog1);
	SPI.transfer(analog2);
	digitalWriteFast2(pinAnalogOut, HIGH);
}

// high level functions
void readButtons(byte n=8){
	// Read the state of the first n buttons (n<=8)
	// Change button state only if dead time for that button is over.
	// Alter global variables: stateButtons, tButtons
	// used by handleButtonsEprime()
	///
	boolean state;
	for(byte bit = 0; bit < n; bit++){
		state = !digitalReadFast2(pinInput[bit]); // true for pressed
		// check input state change, but not within debouncing interval
		if (bitRead(stateButtons, bit) != state && t0 - tButtons[bit] > timeDebounce){
			// set state change
			bitWrite(stateButtons, bit, state);
			// make button insensitive during debouncing interval
			tButtons[bit] = t0;
		}
	}
}

void readButtonsNoDebounce(byte n=8){
	// same as readButtons, but without detection of bouncing
	boolean state;
	for(byte bit = 0; bit < n; bit++){
		state = !digitalReadFast2(pinInput[bit]); // true for pressed
		if (bitRead(stateButtons, bit) != state){
			bitWrite(stateButtons, bit, state);
			tButtons[bit] = t0;
		}
	}
}


void handleButtonsBitsi(){
	// Process input bits
	// Read the state of buttons and send characters A-Ha-h over serial connection in response
	// set global: stateButtons, tButtons
	// used by: bitsi and bitsiextend
	///
	boolean state;
	for (byte bit = 0; bit < 8; bit++){
		state = !digitalReadFast2(pinInput[bit]); // true for pressed
		// check for input state change , but not within debouncing interval
		if (bitRead(stateButtons, bit) != state && t0 - tButtons[bit] > timeDebounce){
			// send A-H for button down or a-h for button up
			if(state){
				Serial.write('A' + bit);
				bitSet(stateButtons, bit);
			} else {
				Serial.write('a' + bit);
				bitClear(stateButtons, bit);
			}
			// make button insensitive during debouncing interval
			tButtons[bit] = t0;
		}
	}
	if(modeExtendLed=='I')
		setLeds(stateButtons);
}

void calibrateSound(){
	soundHigh = 0; // sensor value
	readAnalog(1);
	readAnalog(1);
	for (int i=0; i<5000; i++){
		unsigned int value = readAnalog(channelSound);
		if (value > soundHigh)
			soundHigh = value;
	}
	soundHigh += marginSound;
}

void calibrateVoice(){
	voiceLow = 0xffff;
	voiceHigh = 0;
	for (int i=0; i<5000; i++){
		unsigned int value = readAnalog(channelVoice);
		if(value < voiceLow)
			voiceLow = value;
		if (value > voiceHigh)
			voiceHigh = value;
	} 
	voiceLow -= marginVoice;
	voiceHigh += marginVoice;
}


void writeAnalogA(byte x){
	// write 8 bit value to most significant part of 12 bit DAC-A
	// byte 0, bit 0-3: most significant 4 bits of 12 bit DAC
	// byte 0, bit 4  : 0: disabled, 1: enabled (always 1)
	// byte 0, bit 5  : 0: gain = 2.0, 1: gain = 1.0,  (always 0)
	// byte 0, bit 6  : 0: unbuffered reference, 1: buffered reference (always 0)
	// byte 0, bit 7  : 0: DAC-A, 1: DAC B (always 0 for this function)
	// byte 1: bit 0-7: least significant 8 bits of 12 bit DAC
	// output 0V - 4.3V when gain is set to 2.0, it is very linear
	///
	digitalWriteFast2(pinAnalogOut, LOW);
	// byte 0: most significant 4 bits with settings 
	SPI.transfer(x >> 4 | 0x10);
	// byte 1: least significant 4 bits with most signifant at the lower 4 bits for linear interpolation
	SPI.transfer(byte(x << 4 | x >> 4));
	digitalWriteFast2(pinAnalogOut, HIGH);
}

void writeAnalogB(byte x){
	// write 8 bit value to most significant part of 12 bit DAC-B
	// see writeAnalogA for details 
	///
	digitalWriteFast2(pinAnalogOut, LOW);
	SPI.transfer(x >> 4 | 0x90);
	SPI.transfer(byte(x << 4 | x >> 4));
	digitalWriteFast2(pinAnalogOut, HIGH);
}

void setup(){
	SPI.begin();
	SPI.setBitOrder(MSBFIRST); 
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	pinMode(pinSpeaker, OUTPUT);
	pinMode(pinOutput, OUTPUT);
	pinMode(pinLed, OUTPUT);
	pinMode(pinAnalogIn, OUTPUT); // pin written to to receive analog in from SPI
	pinMode(pinAnalogOut, OUTPUT);
	initDigitalInput();
	setOutput(0);
	setLeds(0);

	// set mode if two buttons pressed
	readButtonsNoDebounce(); // read state of buttons to variable stateButtonss
	if (stateButtons == 0b10000001){ // H + A
		mode = modeBitsi;
		EEPROM.write(addressMode, mode);
	} else if (stateButtons == 0b10000010){ // H + B
		mode = modeBitsiExtend;
		EEPROM.write(addressMode, mode);
 	} else if (stateButtons == 0b10000100){ // H + C
		mode = modePersonal;
		EEPROM.write(addressMode, mode);
 	} else if (stateButtons == 0b10010000){ // H + E
		mode = modeEprime;
		EEPROM.write(addressMode, mode);
	} else {
		mode = EEPROM.read(addressMode);
	}
	if (mode == modeBitsiExtend || mode == modeEprime)
		calibrateSound();
	
	// leave mode as it is unless modeInitial is different from modeNone
	if (modeInitial != modeNone)
		mode = modeInitial;
	
	if (mode == modeBitsi){
		Serial.begin(115200);
		Serial.print("BITSI mode, Ready!");
	} else if (mode == modeBitsiExtend){
		Serial.begin(115200);
		Serial.print("BITSI_extend mode, Ready!");
	} else if (mode == modePersonal){
		Serial.begin(115200);
		Serial.print("Personal mode, Ready!");
	} else if (mode == modeEprime){
		Serial.begin(19200);
		//Serial.write(0x7f);
	}
	if (mode != modeEprime){
		Serial.print(" w:");
		Serial.println(VERSION); // git version tag and hash
	}

	// show mode for 2 s if H pressed
	if (stateButtons & 0b10000000)
		for(byte i=0; i<20; i++){
			setLeds(1<<(mode-1));
			delay(50);
			setLeds(0);
			delay(50);
		}
		
	writeAnalogA(0); // in first call after upload, value is ignored and will always cause 0V
	t0u = micros();
}

void loop(){
	t0 = millis();
	if (mode == modeBitsi){
		if (Serial.available()){
			char c = Serial.read();
			setLeds(c);
			setOutput(c);
		}
		handleButtonsBitsi();
	} else if (mode == modeBitsiExtend){
		while (Serial.available()){
			int i = Serial.read(); // int, -1 for no data available, this does not happen
			unsigned char c = (unsigned char) i;
			if (modeExtendFunction == 'N'){
				// c is first character of a two character bitsi extend command
				if (c == 'M' || c == 'Y' || c == 'Z' || c == 'C' || c == 'A' || c == 'D' || c == 'P' || c == 'X' || c == 'L') 
					modeExtendFunction = c;
				else if (c == 'T') // single character bitsi extend command
					tone(pinSpeaker, 1915, 100); // 1915 Hz, 100 ms square wave on internal speaker
			} else { 
				// modeExtendFunction is the second character of a two character bitsi extend command
				if(modeExtendFunction == 'D'){
					// start detection of sound or voice
					if (c == 'S') 
						modeExtendDetect = 'S';
					else if (c == 'V') 
						modeExtendDetect = 'V';
				} else if(modeExtendFunction == 'M'){
					// send marker
					setOutput(c);
					if(modeExtendLed == 'O')
						setLeds(c);
				} else if(modeExtendFunction == 'P'){
					// like 'M' (send marker) but automatically stops after tPulse
					tPulseEnd = t0 + tPulse;
					setOutput(c);
					if(modeExtendLed == 'O')
						setLeds(c);
				} else if(modeExtendFunction == 'Y'){
					// set value of analog channel 1, 0 - 4.3 V
					writeAnalogA(c);
				} else if(modeExtendFunction == 'Z'){
					// set value of analog channel 2, 0 - 4.3 V
					writeAnalogB(c);
				} else if(modeExtendFunction == 'X'){
					// set pulse duration
					tPulse = (unsigned long) c;
				} else if(modeExtendFunction == 'C'){
					if (c == 'S') {
						calibrateSound();
					} else if (c == 'V') 
						calibrateVoice();
				} else if(modeExtendFunction == 'A'){
					// read one of 4 analog channels (0-5 V) and send output as string
					if (c == '1')
						Serial.println(readAnalog(1)); // "1234\r\n" (0-4096)
					else if (c == '2') 
						Serial.println(readAnalog(2));
					else if (c == '3') 
						Serial.println(readAnalog(3));
					else if (c == '4') 
						Serial.println(readAnalog(4));
				} else if(modeExtendFunction == 'L'){ // change LED, X: off, I: input, O: output
					if (c == 'X'){
						setLeds(0);
						modeExtendLed = 'X';
					} else if (c == 'I'){
						modeExtendLed = 'I';
					} else if (c == 'O'){
						modeExtendLed = 'O';
					}
				}
				// reset to state before first character, even if second character was unrecognized
				modeExtendFunction = 'N'; 
			}
		} // end available bytes loop

		if (t0 >= tPulseEnd){
			// end pulse, started with 'P'
			tPulseEnd = 0xffffffff;
			setOutput(0);
			if(modeExtendLed == 'O')
				setLeds(0);
			setLeds(0);
		}
		handleButtonsBitsi(); // read buttons, set stateButtons, optionally set LEDs
		
		if (modeExtendDetect == 'S'){
			unsigned int value = readAnalog(channelSound);
			if(value > soundHigh){
				modeExtendDetect = 'X';
			}
		} else if(modeExtendDetect == 'V'){
			unsigned int value = readAnalog(channelVoice);
			if(value < voiceLow || value > voiceHigh){
				Serial.print('V');
				modeExtendDetect = 'X';
			}
		}
	} else if (mode == modeEprime){ // end mode extend
		while (Serial.available()){
			byte c = byte(Serial.read()); // int, -1 for no data available (assume that this does not happen)
			// LEDs
			if(c & 0x40){ // lamp bank 1, 
				setLeds((c & 0b00011111) | (stateLeds & 0b11100000)); // set LED A-E, keep LED F-H
				setOutput(c & 0b00011111);
			} else
				setLeds(c << 5 | (stateLeds & 0b00011111)); // set LED F-H, keep LED A-E
			if(c & 0x80) // stream
				if(c & 0x20){ // key bank 1
					calibrateVoice();
					modeEprimeStreaming = 1; 
				} else
					modeEprimeStreaming = 2; 
			else
				modeEprimeStreaming = 0;
		} 
		unsigned tu = micros();
		if(modeEprimeStreaming){
			// wait to approximate requested streaming frequency (800Hz or 1600 Hz)
			if (tu-t0u < frametimeEprime)
				delayMicroseconds(frametimeEprime - tu + t0u);
			readButtons(); // set stateButtons
			// send byte
			if(modeEprimeStreaming==1){ // bank 1, lower 5 buttons
				uint8_t voice = 0;
				unsigned int value = readAnalog(channelVoice);
				if(value < voiceLow || value > voiceHigh)
					voice = 32;
				Serial.print(char((stateButtons & 0b00011111) | voice));
			} else // bank 2, upper 3 buttons
				Serial.print(char(stateButtons >> 5)); 
			t0u = tu;
		}
		
	} // end mode ePrime
}