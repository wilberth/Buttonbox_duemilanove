# Makefile for arduino using arduino-mk on Ubuntu
BOARD_TAG              = atmega328
ARDUINO_DIR            = /usr/share/arduino
TARGET                 = Buttonbox_duemilanove
ARDUINO_LIBS           =
MCU                    = atmega328p
F_CPU                  = 16000000
ARDUINO_PORT           = /dev/ttyUSB0
AVRDUDE_ARD_PROGRAMMER = arduino
OPTIMIZATION_LEVEL     = 3
GIT_VERSION           := $(shell git describe --abbrev=4 --dirty --always --tags)
CPPFLAGS               = -DVERSION=\"$(GIT_VERSION)\"
 
include /usr/share/arduino/Arduino.mk

