from psychopy import core
import serial, time

ser = serial.Serial('com2', 115200, timeout=.01)

for i in range(1,100):
    start = time.time()
    ser.write(chr(i))
    ser.flush();
    #ser.write(chr(i))
    x = ser.read()          # read one byte
    #y = ser.readline()          # read one byte
    end = time.time()
    print ("{:5d} time = {:f}, {:d}".format(i, (end - start) * 1000, ord(x)))
else:
    ser.close()